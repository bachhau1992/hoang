<?php 
class Models_Base {
	protected $_tableName = '';
	protected $_connect;
	public function __construct()
	{
		$this->_connect = new Models_Connect();
	}

	// Get data
	public function getList() {
		$sql = "SELECT * FROM ".$this->_tableName." WHERE del_flag = 0";
		return $this->fetchAll($sql);
	}

	// Search (none pagination)
	public function search ($term = []) {
		$sql = "SELECT * FROM ".$this->_tableName." WHERE del_flag = 0";
		if (!empty($term['name'])) $sql .= " AND name like '".$term['name']."'";
		if (!empty($term['email'])) $sql .= " AND name like '".$term['email']."'";
		return $this->fetchAll($sql);
	}

	public function find($id) {
		$sql = 'SELECT * FROM '. $this->_tableName.' WHERE id = '. $id.' AND del_flag = 0';
		return $this->fetchOne($sql);
	}

	public function delete($id) {
		$sql = "UPDATE ".$this->_tableName." SET del_flag = 1 WHERE id = ".$id;
		return ($this->_connect->execute($sql)) ? true : false;
		// return $sql;
	}

	// Get first data
	public function fetchOne($sql){
		$result = $this->_connect->execute($sql);
		if(!$result){
			return null;	
		}
		return mysqli_fetch_assoc($result);

	}

	// Get all data
	public function fetchAll($sql){
		$result = $this->_connect->execute($sql);
		$list = [];
		while ($row =mysqli_fetch_assoc($result)) {
		  $list[] = $row;
		}
		return $list;
	}

	public function uniqueEditEmail ($id, $email) {
		$sql = "SELECT * FROM ".$this->_tableName." WHERE email = '".$email."' AND id <> '".$id."' AND del_flag = 0";
		$result = $this->fetchOne($sql);
		return ($result) ? false : true;
	}

	// pagination (get data) 
	public function paginate($limit, $start, $row, $arrange) {
		$sql = "SELECT * FROM ".$this->_tableName." WHERE del_flag = 0 ORDER BY ".$row." ".$arrange." LIMIT ".$start.",".$limit;
		return $this->fetchAll($sql);
	}

	public function searchPaginate ($term = [], $limit, $start, $row, $arrange) {
		$sql = "SELECT * FROM ".$this->_tableName." WHERE del_flag = 0";
		if (!empty($term['name'])) $sql .= " AND name like '".$term['name']."'";
		if (!empty($term['email'])) $sql .= " AND email like '".$term['email']."'";
		$sql .= " ORDER BY ".$row." ".$arrange." LIMIT ".$start.",".$limit;
		return $this->fetchAll($sql);
	}
}
