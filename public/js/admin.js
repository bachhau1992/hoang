// Click Menu -> show submenu (Sidebar)
function clickMenu () {
  var x = document.getElementById('treeview-menu');
  if (x.style.display === "block") {
    x.style.display = "none";
  } else {
    x.style.display = "block";
  }
}

// Thay đổi limit phần tử trong 1 trang
function limitChanged(obj) {
  var url;
  var limit = obj.value;
  var currentURL = window.location.toString();
  if (currentURL.indexOf("&page") > 0) {
    // Cắt từ vị trí tìm thấy đến hết
    page = currentURL.slice(currentURL.indexOf("&page"));
    url1 = currentURL.slice(0, currentURL.indexOf("&page"));
  } else {
    page = '';
    url1 = currentURL;
  }
  if (url1.indexOf("&limit") > 0) {
    // Cắt từ đầu đến vị trí tìm thấy
    url2 = url1.slice(0, url1.indexOf("&limit"));
  } else {
    url2 = url1; 
  }
  url = url2+'&limit='+limit+page;
  window.location.href = url;
}

// Sắp xếp phần tử theo DB
function sortDB(obj) {
  var row = obj.trim();
  var arrange;
  var currentURL = window.location.toString();

  if (currentURL.indexOf("&page") > 0) {
    // Cắt từ vị trí tìm thấy đến hết
    page = currentURL.slice(currentURL.indexOf("&page"));
    url1 = currentURL.slice(0, currentURL.indexOf("&page"));
  } else {
    page = '';
    url1 = currentURL;
  }
  if (url1.indexOf("&limit") > 0) {
    // Cắt từ đầu đến vị trí tìm thấy
    limit = url1.slice(url1.indexOf("&limit"));
    url2 = url1.slice(0, url1.indexOf("&limit"));
  } else {
    url2 = url1; 
    limit = '';
  }
  if (url2.indexOf("&arrange") > 0) {
    url3 = url2.slice(0, url2.indexOf("&arrange"));
    arrange = url2.slice(url2.indexOf("&arrange") + 9);
  } else {
    url3 = url2;
  }
  if (url3.indexOf("&row") > 0) {
    url4 = url3.slice(0, url3.indexOf("&row"));
  } else {
    url4 = url3;
  }

  var url;

  if (arrange == null) { 
    url = url4+'&row='+row+'&arrange=desc'+limit+page;
  } else {
    if (arrange == 'desc') {
      arrange = 'asc';
    } else if (arrange == 'asc') {
      arrange = 'desc';
    }
    url = url4+'&row='+row+'&arrange='+arrange+limit+page;
  }
    window.location.href = url;
}

// Get name image
function getImage(obj) {
  // obj.split() => cắt chuỗi obj là C:\fakepath\1.jpg thành 1 mảng [C,:,\,f,a,k,e,p,a,t,h,\,1,.,j,p,g]
  temp = obj.split('\\'); // [C:,fakepath,1.jpg]
  obj = temp[temp.length - 1]; // temp[2] = 1.jpg
  document.getElementById('nameAvatar').innerHTML = obj;
} 
