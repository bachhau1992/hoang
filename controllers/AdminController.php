<?php 
class AdminController extends BaseController
{	
	public function role () {
		// Check session admin_user
		if ($this->hasFlash('admin_user')) {
			$id = $_SESSION['admin_user']['id'];
			$model = new Models_Admin();
			$this->setFlash('admin_user', $model->find($id));
		}

		return $this->render('layouts/role.php', []);
	}

	public function index(){
		$model = new Models_Admin();

		// Check session admin_user
		if ($this->hasFlash('admin_user')) {
			$id = $_SESSION['admin_user']['id'];
			$this->setFlash('admin_user', $model->find($id));
		}

		// Search 
		$term = []; $search = [];
		if (!empty($_GET['name'])) {
			$term['name'] = '%'.$_GET['name'].'%';
			$search['name'] = $_GET['name'];
		}
		if (!empty($_GET['email'])) {
			$term['email'] = '%'.$_GET['email'].'%';
			$search['email'] = $_GET['email'];
		}

		// Unset session URL Admin
		if ($this->hasFlash('urlAdmin')) {
			unset($_SESSION['urlAdmin']);
		}

		// List limit
		$config = new Config_Pagination();
		$listLimit = $config->getLimit(); 

		// Pagination
		$limit = (!empty($_GET['limit'])) ? $_GET['limit'] : $listLimit[0];
		$page = (!empty($_GET['page'])) ? $_GET['page'] : 1;
		$start = ($page - 1) * $limit;
		$links = $config->getLinks();
		$list_class = 'pagination pull-right';

		// Sort
		if (!empty($_GET['row']) && strtolower($_GET['row']) === 'name') {
			$row = 'name';
		} elseif (!empty($_GET['row']) && strtolower($_GET['row']) === 'email') {
			$row = 'email';
		} elseif (!empty($_GET['row']) && strtolower($_GET['row']) === 'status') {
			$row = 'status';
		} else {
			$row = 'id';
		}

		if (isset($_GET['arrange']) && strtoupper($_GET['arrange']) === 'ASC') {
			$arrange = 'ASC';
		} else {
			$arrange = 'DESC';
		}

		// Get data
		if (!empty($term)) {
			$admin = $model->searchPaginate($term, $limit, $start, $row, $arrange);
			$total = count($model->search($term));
		} else {
			$admin = $model->paginate($limit, $start, $row, $arrange); 
			$total = count($model->getList());
		}

		// Phục vụ cho phân trang
		$sort = [
			'row' => (isset($_GET['row'])) ? $_GET['row'] : '',
			'arrange' => (isset($_GET['arrange'])) ? $_GET['arrange'] : ''
		];

		// Tạo phân trang 
		$pagination = $this->createLink('admin', $limit, $page, $total, $links, $list_class, $search, $sort);

		// Biến đếm số phần tử hiển thị và tổng số phần tử
		$count = ['showing' => count($admin), 'total' => $total];

		return $this->render('admin/index.php', ['admin'=> $admin, 'pagination' => $pagination, 'count' => $count, 'listLimit' => $listLimit]);
	} 

	public function create () {
		// Check session admin_user
		if ($this->hasFlash('admin_user')) {
			$id = $_SESSION['admin_user']['id'];
			$model = new Models_Admin();
			$this->setFlash('admin_user', $model->find($id));
		}

		return $this->render('admin/add.php', []);
	}

	public function add () {
		$model = new Models_Admin();

		// Check session admin_user
		if ($this->hasFlash('admin_user')) {
			$id = $_SESSION['admin_user']['id'];
			$this->setFlash('admin_user', $model->find($id));
		}

		// Check validate
		$isValid = new Validators_Admin();

		$data = [
			'name' => trim($_POST['name']),
			'password' => trim($_POST['password']),
			'password_confirmation' => trim($_POST['password_confirmation']),
			'email' => trim($_POST['email']),
			'role_type' => $_POST['role_type']
		];

		// Check image
		$avatar = [];
		if (!empty($_FILES['avatar']['name'])) {
			$avatar = $_FILES['avatar'];
		} elseif ($this->hasFlash('avatar')) {
			$avatar = $this->getFlash('avatar');
		} 

		// Check validate
		if ($isValid->requiredAdmin($data) === false 
			|| $isValid->checkLengthName($_POST['name']) === false 
			|| $isValid->checkPasswordSize($_POST['password']) === false 
			|| $isValid->checkPasswordConfirmation($_POST['password'], $_POST['password_confirmation']) === false) 
		{
			$this->setFlash('add-admin', $_POST);
			$this->setFlash('errors-addAdmin', $isValid->errors());
			if (!empty($avatar)) {
				$_SESSION['add-admin']['file'] = $avatar;
			}			
			return $this->redirect(['c' => 'admin', 'a' => 'create']);
		}

		// Check unique email 
		if ($model->uniqueEmail($_POST['email']) === false) {
			$this->setFlash('add-admin', $_POST);
			if (!empty($avatar)) {
				$_SESSION['add-admin']['file'] = $avatar;
			}
			$this->setFlash('errors-addAdmin', ['email' => 'Email is unique.']);
			return $this->redirect(['c' => 'admin', 'a' => 'create']);
		}

		// Validate success
		date_default_timezone_set('Asia/Ho_Chi_Minh');

		$data['ins_id'] = $_SESSION['admin_user']['id'];
		$data['password'] = md5($data['password']);
		$data['avatar'] = null;

		// Check image
		$errorsImg = [];
		if (!empty($_FILES['avatar']['name'])) {
			$img_name = $_FILES["avatar"]['name'];
			$img_path = "upload/".$img_name;
			// Get type file
			$img_type = strtolower(pathinfo($img_path,PATHINFO_EXTENSION));

			// Check type image, Limit size < 600KB
			if ($isValid->checkImage($img_type) === false || $isValid->checkFileSize('avatar') === false) {
				$errorsImg = $isValid->errors();
			} 

			// Save image
			if (empty($errorsImg)) {
				$img_name = date('YmdHis').".".$img_type;
				$img_path = 'uploads/'.$img_name;
				// Move file from 'tmp' to 'uploads'
				if (move_uploaded_file($_FILES['avatar']['tmp_name'], "$img_path")) {
					$data['avatar'] = $img_name;
				} else {
					$errorsImg['upload'] = "Sorry, there was an error uploading your avatar.";
				}
			}
		} 

		if (!empty($errorsImg)) {
			$this->setFlash('errors-addAdmin', $errorsImg);
			$this->setFlash('add-admin', $_POST);
			if (!empty($avatar)) {
				$_SESSION['add-admin']['file'] = $avatar;
			}
			return $this->redirect(['c' => 'admin', 'a' => 'create']);
		} 

		$admin = $model->create($data); 

		if ($admin) {
			$this->setFlash('admin', 'Add new admin success!');
			return $this->redirect(['c' => 'admin', 'a' => 'index']);
		} else {
			$this->setSession('create-user', $admin);
			return $this->redirect(['c' => 'admin', 'a' => 'create']);
		}
	}

	public function show() {
		// Check session admin_user
		if ($this->hasFlash('admin_user')) {
			$id = $_SESSION['admin_user']['id'];
			$model = new Models_Admin();
			$this->setFlash('admin_user', $model->find($id));
		}

		if (isset($_GET['id'])) {
			$id = $_GET['id'];
			$model = new Models_Admin();
			$admin = $model->find($id);
			
			return $this->render('admin/show.php', ['admin' => $admin]);
		} else {
			include_once('public/404.php'); die;
		}
	}

	public function edit () {
		// Check session admin_user
		if ($this->hasFlash('admin_user')) {
			$id = $_SESSION['admin_user']['id'];
			$model = new Models_Admin();
			$this->setFlash('admin_user', $model->find($id));
		}

		if (isset($_GET['id'])) {
			$id = $_GET['id'];
			$model = new Models_Admin();
			$admin = $model->find($id);
			return $this->render('admin/edit.php', ['admin' => $admin]);
		} else {
			include_once('public/404.php'); die;
		}
	}

	public function update () {
		$model = new Models_Admin();

		// Check session admin_user
		if ($this->hasFlash('admin_user')) {
			$id = $_SESSION['admin_user']['id'];
			$this->setFlash('admin_user', $model->find($id));
		}

		$id = $_GET['id'];

		// Validate
		$isValid = new Validators_Admin();
		$data = [];

		// Get data
		$data['id'] = $id;
		$data['name'] = trim($_POST['name']);
		$data['email'] = trim($_POST['email']);
		$data['role_type'] = $_POST['role_type'] ;

		// Check image tp save value file
		$avatar = [];
		if (!empty($_FILES['avatar']['name'])) {
			$avatar = $_FILES['avatar'];
		} elseif ($this->hasFlash('avatar')) {
			$avatar = $this->getFlash('avatar');
		}
		
		if ($isValid->requiredAdmin($data) === false || $isValid->checkLengthName($_POST['name']) === false) {
			$this->setFlash('update-admin', $_POST);
			if (!empty($avatar)) {
				$_SESSION['update-admin']['file'] = $avatar;
			}
			$this->setFlash('errors-updateAdmin', $isValid->errors());
			return $this->redirect(['c' => 'admin', 'a' => 'edit', 'id' => $id]);
		}

		// Check unique email 
		if ($model->uniqueEditEmail($id, $data['email']) === false) {
			$this->setFlash('update-admin', $_POST);
			if (!empty($avatar)) {
				$_SESSION['update-admin']['file'] = $avatar;
			}
			$this->setFlash('errors-updateAdmin', ['email' => 'Email is unique.']);
			return $this->redirect(['c' => 'admin', 'a' => 'edit', 'id' => $id]);
		}

		// Validate success
		$admin = $model->find($id);
		date_default_timezone_set('Asia/Ho_Chi_Minh');
		$data['avatar'] = $admin['avatar'];
		$data['upd_id'] = $_SESSION['admin_user']['id'];

		// Check image
		$errorsImg = [];
		if (!empty($_FILES['avatar']['name'])) {
			$img_name = $_FILES["avatar"]['name'];
			$img_path = "upload/".$img_name;
			// Get type file
			$img_type = strtolower(pathinfo($img_path,PATHINFO_EXTENSION));

			if (!empty($img_type) && $isValid->checkImage($img_type) === false) {
				$errorsImg = $isValid->errors();
			} 
			if ($isValid->checkFileSize('avatar') === false) {
				$errorsImg = $isValid->errors();
			}

			// Upload image
			if (empty($errorsImg)) {
				$img_name = date('YmdHis').".".$img_type;
				$img_path = 'uploads/'.$img_name;
				
				// Move file upload from tmp to uploads
				if (move_uploaded_file($_FILES['avatar']['tmp_name'], "$img_path")) {
					$data['avatar'] = $img_name;
					// Delete old avatar
					if (!empty($admin['avatar']) && file_exists('uploads/'.$admin['avatar'])) {
						unlink('uploads/'.$admin['avatar']);
					}
				} else {
					$errorsImg['upload'] = "Sorry, there was an error uploading your avatar.";
				}
			}
		} 

		if (!empty($errorsImg)) {
			$this->setFlash('update-admin', $_POST);
			if (!empty($avatar)) {
				$_SESSION['update-admin']['file'] = $avatar;
			}
			$this->setFlash('errors-updateAdmin', $errorsImg);
			return $this->redirect(['c' => 'admin', 'a' => 'edit', 'id' => $id]);
		} 

		// Check new password
		if (!empty($_POST['new_password'])) {
			if (strcmp($_POST['new_password'], trim($_POST['new_password'])) != 0) {
				$this->setFlash('errors-updateAdmin', ['password' => "Password don't contain blank characters."]);
			} elseif ($isValid->required(trim($_POST['password_confirmation']))) {
				$this->setFlash('errors-updateAdmin', ['password_confirmation' => "Password confirmation can't be blank."]);
			} elseif ($isValid->checkPasswordSize(trim($_POST['new_password'])) === false) {
				$this->setFlash('errors-updateAdmin', $isValid->errors());
			} elseif ($isValid->checkPasswordConfirmation(trim($_POST['new_password']), trim($_POST['password_confirmation'])) === false) {
				$this->setFlash('errors-updateAdmin', $isValid->errors());
			} else {
				$data['password'] = trim($_POST['new_password']);
			}
		}

		// Password is not valid.
		if (!empty($_SESSION['errors-updateAdmin'])) {
			$this->setFlash('update-admin', $_POST);
			if (!empty($avatar)) {
				$_SESSION['update-admin']['file'] = $avatar;
			}
			return $this->redirect(['c' => 'admin', 'a' => 'edit', 'id' => $id]);
		}

		// Update admin
		$admin = $model->update($id, $data);
		if ($admin) {
			$this->setFlash('admin', 'Update success!');
			$url = ['c' => 'admin', 'a' => 'index'];
			if ($this->hasFlash('urlAdmin')) {
				$getUrl = $_SESSION['urlAdmin'];
				foreach ($getUrl as $k => $v) {
					$url[$k] = $v;
				}
			}
			return $this->redirect($url);
		} 

		// Update failed.
		$this->setFlash('errors-updateAdmin', ['update' => 'Update failed.']);
		return $this->redirect(['c' => 'admin', 'a' => 'edit', 'id' => $id]);
	}

	public function destroy () {
		if (isset($_GET['id'])) {
			$id = trim($_GET['id']);

			$model = new Models_Admin();

			// Check session admin_user
			if ($this->hasFlash('admin_user')) {
				$idAdmin = $_SESSION['admin_user']['id'];
				$this->setFlash('admin_user', $model->find($idAdmin));
			}

			$admin = $model->find($id);

			if (empty($admin)) {
				$this->setFlash('delete-admin', 'Delete failed.');
			} else {
				// Delete avatar
				if (!empty($admin['avatar']) && file_exists('uploads/'.$admin['avatar'])) {
					unlink('uploads/'.$admin['avatar']);
				}

				$model->delete($id);
				$this->setFlash('admin', 'Delete success!');
			}
			$url = ['c' => 'admin', 'a' => 'index'];
			if ($this->hasFlash('urlAdmin')) {
				$getUrl = $_SESSION['urlAdmin'];
				foreach ($getUrl as $k => $v) {
					$url[$k] = $v;
				}
			}
			return $this->redirect($url);
		} else {
			include_once('public/404.php'); die;
		}
	}
}
