<?php class UsersController extends BaseController{
	public function index()	{
		// Check session admin_user
		if ($this->hasFlash('admin_user')) {
			$id = $_SESSION['admin_user']['id'];
			$model = new Models_Admin();
			$this->setFlash('admin_user', $model->find($id));
		}

		$model = new Models_User();

		// Get term
		$term = []; $search = [];
		if (!empty($_GET['name'])) {
			$term['name'] = '%'.$_GET['name'].'%';
			$search['name'] = $_GET['name'];
		}

		if (!empty($_GET['email'])) {
			$term['email'] = '%'.$_GET['email'].'%';
			$search['email'] = $_GET['email'];
		}

		// Delete session url (info search, limit)
		if ($this->hasFlash('urlUser')) unset($_SESSION['urlUser']);

		// CONFIG
		$config = new Config_Pagination();
		$listLimit = $config->getLimit();

		// Pagination and display
		$limit = (!empty($_GET['limit'])) ? $_GET['limit'] : $listLimit[0];
		$page = (!empty($_GET['page'])) ? $_GET['page'] : 1;
		$start = ($page - 1) * $limit;
		$links = $config->getLinks();
		$list_class = 'pagination pull-right';

		// Sort
		if (!empty($_GET['row']) && strtolower($_GET['row']) === 'name') {
			$row = 'name';
		} elseif (!empty($_GET['row']) && strtolower($_GET['row']) === 'email') {
			$row = 'email';
		} elseif (!empty($_GET['row']) && strtolower($_GET['row']) === 'status') {
			$row = 'status';
		} else {
			$row = 'id';
		}

		if (isset($_GET['arrange']) && strtoupper($_GET['arrange']) === 'ASC') {
			$arrange = 'ASC';
		} else {
			$arrange = 'DESC';
		}

		if (!empty($term)) {
			$users = $model->searchPaginate($term, $limit, $start, $row, $arrange);
			$total = count($model->search($term));
		} else {
			$users = $model->paginate($limit, $start, $row, $arrange);
			$total = count($model->getList());
		}

		$sort = [
			'row' => (isset($_GET['row'])) ? $_GET['row'] : '',
			'arrange' => (isset($_GET['arrange'])) ? $_GET['arrange'] : ''
		];

		$pagination = $this->createLink('users', $limit, $page, $total, $links, $list_class, $search, $sort);

		$count = ['showing' => count($users), 'total' => $total];

		return $this->render('users/index.php', ['users'=> $users, 'pagination' => $pagination, 'count' => $count, 'listLimit' => $listLimit]);
	} 

	public function edit () {
		// Check session admin_user
		if ($this->hasFlash('admin_user')) {
			$id = $_SESSION['admin_user']['id'];
			$model = new Models_Admin();
			$this->setFlash('admin_user', $model->find($id));
		}

		if (isset($_GET['id'])) {
			$model = new Models_User();
			$id = trim($_GET['id']);
			$user = $model->find($id);

			if (empty($user)) {
				$this->setFlash('errors-user', 'Update failed.');
			} else {
				$data['upd_id'] = $_SESSION['admin_user']['id'];
				$data['status'] = ($user['status'] == 1) ? 2 : 1;
				$model->update($id, $data);
				$this->setFlash('users', 'Update success!');
			}
			
			$url = ['c' => 'users', 'a' => 'index'];
			if ($this->hasFlash('urlUser')) {
				$getUrls = $_SESSION['urlUser'];
				foreach ($getUrls as $k => $v) {
					$url[$k] = $v;
				}
			}
			return $this->redirect($url);
		} else {
			include_once('public/404.php'); die;
		}
	}

	public function destroy() {
		if (isset($_GET['id'])) {
			// Check session admin_user
			if ($this->hasFlash('admin_user')) {
				$idAdmin = $_SESSION['admin_user']['id'];
				$model = new Models_Admin();
				$this->setFlash('admin_user', $model->find($idAdmin));
			}
			
			$id = trim($_GET['id']);
			$model = new Models_User();

			// Link redirect index (save info)
			$url = ['c' => 'users', 'a' => 'index'];
			if ($this->hasFlash('urlUser')) {
				$getUrls = $_SESSION['urlUser'];
				foreach ($getUrls as $k => $v) {
					$url[$k] = $v;
				}
			}

			// Check id này có tồn tại không
			if (empty($model->find($id))) {
				$this->setFlash('errors-user', "Delete failed, ID doesn't exist.");
			} else {
				$user = $model->delete($id); 
				if ($user) {
					$this->setFlash('users', "Delete success!");
					return $this->redirect($url);
				} else {
					$this->setFlash('errors-user', 'Delete failed.');
				}
			}
			return $this->redirect($url);	
		} else {
			include_once('public/404.php'); die;
		}
	}

	public function info () {
		if ($this->hasFlash('user_fb')) {
			$info = $_SESSION['user_fb'];
			$info['logoutURL'] = (isset($_SESSION['logoutURL'])) ? $_SESSION['logoutURL'] : 'index.php?c=login&a=logoutFB'; 
			return $this->render('users/info.php', ['info' => $info]);
		} else {
			return $this->redirect(['c' => 'login', 'a' => 'loginFB']);
		}
	}
}
