<?php
class BaseController
{	
	// Get view
	public function render ($viewPath, $data = [])
	{
		ob_start();
		extract($data);
		include_once 'views/'.$viewPath;
		$result = ob_get_contents();
		ob_end_clean();
		return $result;
	}

	// Redirect to other page
	public function redirect ($params = []) {
		$link = http_build_query($params);
		header("location:index.php?".$link);
	}
	
	// Set flash
	public function setFlash ($session, $content) {
		return $_SESSION[$session] = $content;
	}
	// Get flash 
	public function getFlash ($session) {
		$get = $_SESSION[$session];
		unset($_SESSION[$session]);
		return $get;
	}
	// Check flash
	public function hasFlash ($session) {
		return (isset($_SESSION[$session])) ? true : false;
	}

	public function getSession ($session) {
		return $_SESSION[$session];
	}

	public function setSession ($session, $content) {
		return $_SESSION[$session] = $content;
	}

	public function createLink($table, $limit, $page, $total, $links, $list_class, $termSearch = [], $termSort = []) {
		$last = ceil($total / $limit);
		$start = ($page - $links > 0) ? $page - $links : 1;
		$end = ($page + $links < $last) ? $page + $links : $last;
		
		$name = (!empty($termSearch['name'])) ? $termSearch['name'] : '';
		$email = (!empty($termSearch['email'])) ? $termSearch['email']: '';
		$search = (!empty($termSearch)) ? '&name='.$name.'&email='.$email : '';

		$row = (!empty($termSort['row'])) ? $termSort['row'] : '';	
		$arrange = (!empty($termSort['arrange'])) ? $termSort['arrange'] : '';
		$sort = (!empty($termSort)) ? '&row='.$row.'&arrange='.$arrange : '';

		$html = '<ul class="'.$list_class.'" >';
		$class = ($page == 1) ? 'disabled' : '';
		$html .= '<li class="'.$class.'" ><a href="index.php?c='.$table.'&a=index'.$search.$sort.'&limit='.$limit.'&page='.($page - 1).'">&laquo;</a>';
		 if ( $start > 1 ) {
	        $html .= '<li><a href="index.php?c='.$table.'&a=index'.$search.$sort.'&limit='.$limit.'&page=1">1</a></li>';
	        $html .= '<li class="disabled"><span>...</span></li>';
	    }
	 
	    for ( $i = $start ; $i <= $end; $i++ ) {
	        $class = ( $page == $i ) ? "active" : "";
	        $html .= '<li class="'.$class.'"><a href="index.php?c='.$table.'&a=index'.$search.$sort.'&limit='.$limit.'&page='.$i.'">'.$i.'</a></li>';
	    }
	 
	    if ( $end < $last ) {
	        $html .= '<li class="disabled"><span>...</span></li>';
	        $html .= '<li><a href="index.php?c='.$table.'&a=index'.$search.$sort.'&limit='.$limit.'&page='.$last.'">'.$last.'</a></li>';
	    }
	 
	    $class = ( $page == $last ) ? "disabled" : "";
	    $last_index = ($page == $last) ? $last : ($page + 1);
	    $html .= '<li class="'.$class.'"><a href="index.php?c='.$table.'&a=index'.$search.$sort.'&limit='.$limit.'&page='.$last_index.'">&raquo;</a></li>';
	 
	    $html .= '</ul>';
	 
	    return $html;
	}
}

?>
