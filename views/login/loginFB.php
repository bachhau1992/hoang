<?php
if (isset($_SESSION['user_fb'])) {
	header("location:index.php?c=users&a=info");
}
?>

<!DOCTYPE html>
<html>
<head>
	<title>Website | Login FB</title>
	<!-- Css -->
	<link rel="stylesheet" type="text/css" href="public/css/login.css">
	<link rel="stylesheet" type="text/css" href="public/css/font-awesome.min.css">
</head>
<body>
	<?php 
	if ($this->hasFlash('confirm-loginFB')) {
		echo '<span style="color:green;">'.$this->getFlash('confirm-loginFB').'</span><br>';
	}

	if ($this->hasFlash('registerFB')) {
		echo '<span style="color:orange;">'.$this->getFlash('registerFB').'</span> You want to <a href="index.php?c=login&a=registerFB">register</a>.<br>';
	}

	if ($this->hasFlash('errors-loginFB')) {
		echo '<span style="color:red;"> <i class="fa fa-exclamation-triangle"></i> '.$this->getFlash('errors-loginFB').'</span><br>';
	}
	?>
	<!-- <div class="login-fb"> -->
		<a href="<?php echo htmlspecialchars($loginURL);?>"><img src="public/images/facebook-sign-in-button.png" width="133px" height="50px"></a>
	<!-- </div> -->
</body>
</html>
