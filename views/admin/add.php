<?php 
if (isset($_SESSION['admin_user'])) {
	if ($_SESSION['admin_user']['role_type'] == 1) {
		require 'views/layouts/top.php';
?>
			
<section class="content-header">
	<h1>
		Admin <small>Add new admin</small> 
		<small class="pull-right"><i class="fa fa-dashboard"></i> Home > Admin > Add</small> 
	</h1>
</section>

<?php  
	if (isset($_SESSION['errors-addAdmin'])) {
		?>
		<div class="alert alert-danger">
			<ul>
			<?php
			$errors = $this->getFlash('errors-addAdmin');
			foreach ($errors as $error) {
				echo '<li>'.$error.'</li>';
			}
			?>
			</ul>
		</div>
		<?php
	}
?>

<!-- Content body -->
<section class="content">
	<div class="box">
		<div class="box-header"><span>Add new admin</span></div>
		<div class="box-body">
			<form method="POST" action="index.php?c=admin&a=add" enctype="multipart/form-data">	
				<input type="hidden" name="ins_id" value="<?php echo $_SESSION['admin_user']['id'];?>">
				<?php 
					$oldValues = [];
					if (isset($_SESSION['add-admin'])) {
						$oldValues = $this->getFlash('add-admin');
					}
					if (isset($_SESSION['avatar'])) {
						unset($_SESSION['avatar']);
					}
				?>			
				<div class="form-group width50">
					<label for="name">Name: <span class="red"> <i class="fa fa-asterisk"></i> </span></label><br>
					<input type="text" name="name" class="form-control" value="<?php echo (!empty($oldValues['name'])) ? $oldValues['name'] : ''; ?>">
				</div>
				<div class="form-group width50">
					<label for="password">Password: <span class="red"> <i class="fa fa-asterisk"></i> </span></label><br>
					<input type="password" name="password" class="form-control">
				</div>
				<div class="form-group width50">
					<label for="password_confirmation">Password confirmation: <span class="red"> <i class="fa fa-asterisk"></i> </span></label><br>
					<input type="password" name="password_confirmation" class="form-control">
				</div>
				<div class="form-group width50">
					<label for="email">Email: <span class="red"> <i class="fa fa-asterisk"></i> </span></label><br>
					<input type="email" name="email" class="form-control" value="<?php echo (!empty($oldValues['email'])) ? $oldValues['email'] : ''; ?>">
				</div>
				<div class="form-group width50">
					<label for="avatar">Avatar:</label><br>
					<div class="img-file">
						<button type="button" class="btn btn-danger btn-file"><label for="avatar"><i class="fa fa-image"></i></label></button> 
						<input type="file" name="avatar" id="avatar" style="display: none;" onchange="getImage(this.value)"> 
						<span id="nameAvatar">
							<?php 
							if (!empty($oldValues['file']['name'])) { 
								echo $oldValues['file']['name'];
								$this->setFlash('avatar', $oldValues['file']); 
							} else {
							  	echo 'Choose an avatar...'; 
							}
							?>
						</span>
					</div>	
				</div>
				<div class="form-group width50">
					<label for="role_type">Role: <span class="red"> <i class="fa fa-asterisk"></i> </span></label><br>
					<select name="role_type">
						<option value="">--- Select status ---</option>
						<option value="1" <?php echo (!empty($oldValues['role_type']) && ($oldValues['role_type'] == 1)) ? 'selected' : ''; ?> >Super admin</option>
						<option value="2" <?php echo (!empty($oldValues['role_type']) && ($oldValues['role_type'] == 2)) ? 'selected' : ''; ?> >Admin</option>
					</select>
				</div>

				<div class="form-group width100 text-center">
					<button type="submit" name="save" class="btn btn-danger" value="editUser">Save</button>
					<button type="button" class="btn-a btn-primary"><a href="index.php?c=admin&a=index">Cancel</a></button>
				</div>
			</form>
			<div class="clear"></div>
		</div>		
	</div>
</section>			
			
<?php
		require 'views/layouts/bottom.php';
	} else {
			header("location:index.php?c=admin&a=role");
	}
} else {
	header("location:index.php?c=login&a=show");
}
?>
