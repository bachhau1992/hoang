<?php 
if (isset($_SESSION['admin_user'])) {
	if ($_SESSION['admin_user']['role_type'] == 1 || $_SESSION['admin_user']['id'] == $_GET['id']) {
		require 'views/layouts/top.php';
?>
			
<section class="content-header">
	<h1>
		Admin <small>Edit information admin</small>
		<small class="pull-right"><i class="fa fa-dashboard"></i> Home > Admin > Edit</small>
	</h1>
</section>

<?php  
	if ($this->hasFlash('errors-updateAdmin')) {
		?>
		<div class="alert alert-danger">
			<ul>
			<?php
			$errors = $this->getFlash('errors-updateAdmin');
			foreach ($errors as $error) {
				echo '<li>'.$error.'</li>';
			}
			?>
			</ul>
		</div>
		<?php
	}
?>

<!-- Content body -->
<section class="content">
	<div class="box">
		<div class="box-header"><span>Edit information admin</span></div>
		<div class="box-body">
			<?php  
			if (empty($admin)) {
				echo 'Not found, <a href="index.php?c=admin&a=index">return</a>.';
			} else {
			?>
				<form method="POST" action="index.php?c=admin&a=update&id=<?php echo $admin['id'];?>"" enctype="multipart/form-data">	
					<input type="hidden" name="upd_id" value="<?php echo $_SESSION['admin_user']['id'];?>">
					<?php 
						$oldValues = [];
						if (isset($_SESSION['update-admin'])) {
							$oldValues = $this->getFlash('update-admin');
						}
						if (isset($_SESSION['avatar'])) {
							unset($_SESSION['avatar']);
						}
					?>			
					<div class="form-group width50">
						<label for="name">Name: <span class="red"> <i class="fa fa-asterisk"></i> </span></label><br>
						<input type="input" name="name" class="form-control" value="<?php echo (isset($oldValues['name'])) ? $oldValues['name'] : $admin['name']; ?>">
					</div>
					<div class="form-group width50">
						<label for="password">New password: </label><br>
						<input type="password" name="new_password" class="form-control">
					</div>
					<div class="form-group width50">
						<label for="email">Email: <span class="red"> <i class="fa fa-asterisk"></i> </span></label><br>
						<input type="email" name="email" class="form-control" value="<?php echo (isset($oldValues['email'])) ? $oldValues['email'] : $admin['email']; ?>">
					</div>
					<div class="form-group width50">
						<label for="password_confirmation">Password confirmation: </label><br>
						<input type="password" name="password_confirmation" class="form-control">
					</div>
					<div class="form-group width50">
						<label for="role_type">Role: <span class="red"> <i class="fa fa-asterisk"></i> </span></label><br>
						<select name="role_type">
							<?php 
							$se_super = ''; $se_admin = ''; $se_none = '';
							if (isset($oldValues['role_type'])) {
								$role = $oldValues['role_type'];
								if (empty($role)) {
									$se_none = 'selected';
								} elseif ($role == 1) {
									$se_super = 'selected';
								} elseif ($role == 2) {
									$se_admin = 'selected';
								}
							} elseif ($admin['role_type'] == 1) {
								$se_super = 'selected';
							} else {
								$se_admin = 'selected';
							}
							
							?>
							<option value="" <?php echo $se_none; ?> >--- Select status ---</option>
							<option value="1" <?php echo $se_super; ?> >Super admin</option>
							<option value="2" <?php echo $se_admin; ?> >Admin</option>
						</select>
					</div>
					
					<div class="form-group width50">
						<label for="avatar">Avatar:</label><br>
						<div class="img-file">
						<button type="button" class="btn btn-danger"><label for="avatar"><i class="fa fa-image"></i></label></button> 
						<input type="file" name="avatar" id="avatar" style="display: none;" onchange="getImage(this.value)"> 
						<span id="nameAvatar">
							<?php 
							if (!empty($oldValues['file']['name'])) { 
								echo $oldValues['file']['name'];
								$this->setFlash('avatar', $oldValues['file']); 
							} elseif (!empty($admin['avatar'])) {
								echo $admin['avatar'];
							} else {
							  	echo 'Choose an avatar...'; 
							}
							?>
						</span>
					</div>	
					</div>
					<input type="hidden" name="upd_id" value="<?php echo $_SESSION['admin_user']['id']; ?>">
			
					<div class="form-group width100 text-center">
						<button type="submit" name="save" class="btn btn-danger" value="editUser">Save</button>
						<button type="button" class="btn-a btn-primary"><a href="index.php?c=admin&a=index">Cancel</a></button>
					</div>
				</form>
				<?php 
			}
			?>
			<div class="clear"></div>
		</div>		
	</div>
</section>			
			
<?php
		require 'views/layouts/bottom.php';
	} else {
		header("location:index.php?c=admin&a=role");
	}
} else {
	header("location:index.php?c=login&a=show");
}
?>
