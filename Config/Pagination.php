<?php 
// Cấu hình các chỉ số mặc định để phục vụ phân trang
class Config_Pagination {
	public $limit = [10,25,50];
	public $links = 5;

	public function getLimit() {
		return $this->limit;
	}

	public function getLinks() {
		return $this->links;
	}
}

?>